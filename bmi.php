<?php

require_once("vendor/autoload.php");
$latte = new Latte\Engine;

$latte->setTempDirectory('temp');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Zpracovani formulare po odeslani
    $age = $_POST['age'];
    $weight = $_POST['weight'];
    $heightInCentimeters = $_POST['height'];

    // Convert height to meters for BMI calculation
    $height = $heightInCentimeters / 100;

    // Vypocet BMI
    $bmi = $weight / ($height * $height);

    // Kategorizace BMI podle veku
    $bmiCategory = getBMICategory($age, $bmi);

    // Preneseni dat do sablony
    $params = [
        'bmi' => $bmi,
        'bmiCategory' => $bmiCategory,
    ];

    // Rendrovani sablony
    $latte->render('template/bmiTemplate.latte', $params);
} else {
    // Zobrazi se formular pro zadani veku, vahy a vysky
    $latte->render('template/bmiForm.latte');
}

function getBMICategory($age, $bmi)
{
    $ageCategories = [
        24 => '18 - 24',
        34 => '25 - 34',
        44 => '35 - 44',
        54 => '45 - 54',
        64 => '55 - 64',
        INF => '65+',
    ];

    $bmiCategories = [
        '< 19' => 'Podváha',
        '19 - 24' => 'Optimální váha',
        '24 - 29' => 'Nadváha',
        '29 - 39' => 'Obezita',
        '> 39' => 'Silná obezita',
    ];

    // Find the appropriate age range
    foreach ($ageCategories as $maxAge => $ageRange) {
        if ($age <= $maxAge) {
            // Find the appropriate BMI category based on age range
            foreach ($bmiCategories as $range => $category) {
                if ($bmi >= $range) {
                    return $category;
                }
            }
        }
    }

    return '';
}