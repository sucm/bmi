<?php

use Latte\Runtime as LR;

/** source: template/bmiTemplate.latte */
final class Template3a4a83de1b extends Latte\Runtime\Template
{
	public const Source = 'template/bmiTemplate.latte';

	public const Blocks = [
		['head' => 'blockHead', 'content' => 'blockContent'],
	];


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
';
		$this->renderBlock('head', get_defined_vars()) /* line 6 */;
		echo '    <title>Your BMI Calculator</title>
</head>
<body>
';
		$this->renderBlock('content', get_defined_vars()) /* line 12 */;
		echo '</body>
</html>
';
	}


	/** {block head} on line 6 */
	public function blockHead(array $ʟ_args): void
	{
		echo '        <link rel="stylesheet" type="text/css" href="template/styles.css">
';
	}


	/** {block content} on line 12 */
	public function blockContent(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		unset($ʟ_args);

		echo '    <div class="container">
        <h2>Výsledek BMI</h2>
        <p>Vaše BMI hodnota je: ';
		echo LR\Filters::escapeHtmlText($bmi) /* line 15 */;
		echo '</p>
        <p>Kategorie BMI: ';
		echo LR\Filters::escapeHtmlText($bmiCategory) /* line 16 */;
		echo '</p>

        <!-- Button to go back to the calculator -->
        <form method="get" action="">
            <button type="submit">Zpět k výpočtu BMI</button>
        </form>
    </div>
';
	}
}
