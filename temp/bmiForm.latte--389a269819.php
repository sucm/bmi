<?php

use Latte\Runtime as LR;

/** source: template/bmiForm.latte */
final class Template389a269819 extends Latte\Runtime\Template
{
	public const Source = 'template/bmiForm.latte';

	public const Blocks = [
		['head' => 'blockHead', 'content' => 'blockContent'],
	];


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		$this->renderBlock('head', get_defined_vars()) /* line 1 */;
		echo "\n";
		$this->renderBlock('content', get_defined_vars()) /* line 5 */;
	}


	/** {block head} on line 1 */
	public function blockHead(array $ʟ_args): void
	{
		echo '    <link rel="stylesheet" type="text/css" href="template/styles.css">
';
	}


	/** {block content} on line 5 */
	public function blockContent(array $ʟ_args): void
	{
		echo '    <h2>Zadejte své údaje pro výpočet BMI</h2>
    <form method="post" action="">
        <label>Věk:
            <input type="number" name="age" required>
        </label><br>

        <label>Váha (kg):
            <input type="number" name="weight" step="0.1" required>
        </label><br>

        <label>Výška (cm):
            <input type="number" name="height" step="0.01" required>
        </label><br>

        <button type="submit">Vypočítat BMI</button>
    </form>
';
	}
}
